local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
PROMPT='
$fg[yellow]$(get_pwd) $(node_version) $(git_prompt_info)      🕗  %{$fg_bold[cyan]%}%*%{$reset_color%} $(prompt_battery)%{$reset_color%}
${ret_status}%{$reset_color%}'

RPROMPT='🕗  %{$fg_bold[cyan]%}%*%{$reset_color%} $(prompt_battery)%{$reset_color%}    '
RPROMPT=''

# Must use Powerline font, for \uE0A0 to render.
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}\uE0A0 "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="$fg[red]+"
ZSH_THEME_GIT_PROMPT_CLEAN="$fg[green]"

# NODE
NODE_PREFIX="["
NODE_SUFFIX="]"
NODE_SYMBOL="⬢ "
NODE_DEFAULT_VERSION=""
NODE_COLOR="%{$fg[green]%}"

function get_pwd() {
  echo "${PWD/$HOME/~}"
}

function git_remote_status() {
  reg_ahead="is ahead of .* by ([0-9]+) commit"
  [[ $(git status) =~ $reg_ahead ]] && echo ↑${match[1]}
  reg_ahead_begind="and have ([0-9]+) and ([0-9]+) different commits each"
  [[ $(git status) =~ $reg_ahead_begind ]] && echo ↑${match[1]}↓${match[2]}
  reg_behind="is behind .* by ([0-9]+) commit"
  [[ $(git status) =~ $reg_behind ]] && echo ↓${match[1]}
}

function git_last_commit() {
  echo $(git log --pretty=format:"%an %h \"%s\"" -1 2> /dev/null | cut -c 1-90)
}

function git_last_commit_author() {
  echo $(git log --pretty=format:"%an" -1 2> /dev/null)
}

function git_last_commit_hash() {
  echo $(git log --pretty=format:"%h" -1 2> /dev/null)
}

function git_last_commit_message() {
  echo $(git log --pretty=format:"\"%s\"" -1 2> /dev/null | cut -c 1-70)
}

function git_prompt_info() {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  echo "\
$ZSH_THEME_GIT_PROMPT_PREFIX\
$(parse_git_dirty)$(current_branch)\
$ZSH_THEME_GIT_PROMPT_SUFFIX \
$fg_bold[blue]$(git_remote_status)%{$reset_color%} \
$(git_last_commit_author) \
$fg_bold[blue]$(git_last_commit_hash)%{$reset_color%} \
$(git_last_commit_message) \
"
}

_exists() {
  command -v $1 > /dev/null 2>&1
}

# NODE
# Show current version of node, exception system.
node_version() {
  # Show NODE status only for JS-specific folders
  [[ -f package.json || -d node_modules || -n *.js(#qN) ]] || return

  local node_version

  if _exists nvm; then
    node_version=$(nvm current 2>/dev/null)
    [[ $node_version == "system" || $node_version == "node" ]] && return
  elif _exists nodenv; then
    node_version=$(nodenv version-name)
    [[ $node_version == "system" || $node_version == "node" ]] && return
  elif _exists node; then
    node_version=$(node -v 2>/dev/null)
    [[ $node_version == $NODE_DEFAULT_VERSION ]] && return
  else
    return
  fi

  echo "${NODE_COLOR}${NODE_PREFIX}${NODE_SYMBOL}${node_version}$NODE_SUFFIX"
}

# Battery Level
prompt_battery() {
  function battery_is_not_charging() {
    [[ $(ioreg -rc AppleSmartBattery | grep -c '^.*"ExternalConnected"\ =\ No') == 1 ]]
  }

  function battery_pct() {
    local smart_battery_status="$(ioreg -rc "AppleSmartBattery")"
    typeset -F maxcapacity=$(echo $smart_battery_status | grep '^.*"MaxCapacity"\ =\ ' | sed -n -e 's/^.*"MaxCapacity"\ =\ //p')
    typeset -F currentcapacity=$(echo $smart_battery_status | grep '^.*"CurrentCapacity"\ =\ ' | sed -n -e 's/^.*CurrentCapacity"\ =\ //p')
    integer i=$(((currentcapacity/maxcapacity) * 100))
    echo $i
  }

  local charging
  battery_is_not_charging && charging="🔋" || charging="🔌"
  local b=$(battery_pct)
  local color
  if [ $b -gt 50 ] ; then
    color="%{$fg[green]%}"
  elif [ $b -gt 20 ] ; then
    color="%{$fg[yellow]%}"
  else
    color="%{$fg[red]%}"
  fi
  echo "${charging} ${color}${b}%%"
}
