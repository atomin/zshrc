# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="robbyrussell"
ZSH_THEME="tom"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git yarn npm osx web-search wd docker)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias zshconfig="code ~/.zshrc"
alias gitconfig="code ~/.gitconfig"
alias envsettings="code ~/settings"
alias settings="code ~/settings"
alias ohmyzsh="code ~/.oh-my-zsh"
# alias sed="sed -n" # breaks wd (warp directory oh-my-zsh extension)
alias grep="grep -E -i --color=auto"
alias grepc="grep -E -i --color=always"
alias flowwatch="fswatch -o ./ | xargs -n1 -I{} sh -c 'clear; printf \"\033[3J\" && npx flow'"
alias aliases="grep -E \"^alias|^# alias\" ~/.zshrc | sed -nE \"s/alias |# alias //p\" | sort | grep -E \"^[a-z-]+\""
alias f="find"
alias envs="printenv"
# alias sshconfig - open sshconfig, -e flag for edit
# alias wcode - open warp dicrecory (wd) in vs code
# alias gen-ssh email - generate ssh key

alias docker_stop_all='docker stop $(docker ps -q)'
alias codestr=native2ascii
alias decodestr=native2ascii --reverse
alias copy=cp
alias cms-run="osascript ~/settings/iterm2_cms.scpt"

export NVM_DIR="$HOME/.nvm"
. "$NVM_DIR/nvm.sh"

# autoload -U add-zsh-hook
# load-nvmrc() {
#   if [[ -f .nvmrc && -r .nvmrc ]]; then
#     nvm use
#   elif [[ $(nvm version) != $(nvm version default)  ]]; then
#     echo "Reverting to nvm default version"
#     nvm use default
#   fi
# }
# add-zsh-hook chpwd load-nvmrc
# load-nvmrc


export EDITOR='nano'

export SCALA_HOME=/usr/local/share/scala
export PATH=$PATH:$SCALA_HOME/bin
# export PATH=/usr/local/apache-maven-3.5.4/bin:$PATH

. ~/settings/myfunctions.sh

. ~/settings/secrets.sh

# projects related stuff

export ESLINT_AUTOFIX=false
export SBT_OPTS='-Xss2m -Xmx2048m'
# export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_202.jdk/Contents/Home
# export PATH="/usr/local/opt/maven@3.3/bin:$PATH"
export PATH="/usr/local/opt/openjdk@11/bin:$PATH"
export REDUX_DEBUG="false"
export NODE_OPTIONS=--max_old_space_size=8192
export HUSKY_SKIP_HOOKS=1


# WR
export WR_CMS_DISABLE_TYPESCRIPT_CHECK=true

# if command -v pyenv 1>/dev/null 2>&1; then
#   eval "$(pyenv init -)"
# fi

# echo "[[ $commands[kubectl] ]] && source <(kubectl completion zsh)"
 
 # Load completion files from the ~/.zsh directory.
zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.bash
fpath=(~/.zsh $fpath)

autoload -Uz compinit && compinit