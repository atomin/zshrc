import glob

path_prefix = "/Users/alexey_tomin/projects/ticketmaster/tmol-web-spring/webapp/target/tmol-webapp/"

def replace(path, prefix=path_prefix):
    files = list(glob.iglob(path, recursive=True))
    print(path, len(files))

    for file in files:
        print(file)
        with open(file) as reader:
            fileContent = reader.read()
            debugLine = f"<!-- template_debug {file.replace(prefix, '')} -->"
            with open(file, "w") as writer:
                writer.write(f"{debugLine}\n{fileContent}")


replace("/Users/alexey_tomin/projects/ticketmaster/tmol-web-spring/webapp/target/tmol-webapp/templates/**/*.html")
replace("/Users/alexey_tomin/projects/ticketmaster/tmol-web-spring/webapp/target/tmol-webapp/sites/**/*.html")

# replace(
#     "/Users/alexey_tomin/projects/ticketmaster/tmol-web-spring/tmol-front-end/edp/src/main/webapp/templates/edp/**/*.html",
#     "/Users/alexey_tomin/projects/ticketmaster/tmol-web-spring/tmol-front-end/edp/src/main/webapp")
# replace("/Users/alexey_tomin/projects/ticketmaster/tmol-web-spring/tmol-front-end/edp/src/main/webapp/sites/**/*.html",
#         "/Users/alexey_tomin/projects/ticketmaster/tmol-web-spring/tmol-front-end/edp/src/main/webapp")
