#!/bin/bash -v -x

# paris functions
paris_upload_local_data_corilus() {
  wd engine
  dataset=${2-austria}
  ./tools/upload/configuration-and-dataset.sh -v -e http://localhost:8090/engine -s $dataset -t $dataset -f Corilus -d $1
}

paris_upload_local_data_json() {
  wd engine
  dataset=${2-austria}
  ./tools/upload/configuration-and-dataset.sh -v -e http://localhost:8090/engine -s $dataset -d $1
}

paris_upload_local_settings() {
  wd engine
  settingstype=${1-switzerland}
  mkdir -p ~/projects/paris/temp/temp_config
  \rm -r ~/projects/paris/temp/temp_config
  ./tools/upload/configuration-and-dataset.sh -v -e http://localhost:8090/engine -t $settingstype -c ../temp/temp_config
}

paris_upload_belgium_data() {
  wd engine
  dataset=${3-belgium}
  ./tools/upload/configuration-and-dataset.sh -v -l austria -p changeit -e $2 -s $dataset -R $dataset -t $dataset -r -f Corilus -d $1
}

paris_upload_switzerland_data() {
  wd engine
  dataset=${3-switzerland}
  ./tools/upload/configuration-and-dataset.sh -v -l switzerland -p changeit -e $2 -s $dataset -R $dataset -t $dataset -r -d $1
}

paris_upload_austria_data() {
  wd engine
  dataset=${3-austria}
  ./tools/upload/configuration-and-dataset.sh -v -l austria -p changeit -e $2 -s $dataset -R $dataset -t $dataset -r -d $1
}

paris_upload_switzerland_settings_on_test() {
  mkdir -p ../temp/temp_config
  \rm -r ../temp/temp_config
  ./tools/upload/configuration-and-dataset.sh -v -l switzerland -p changeit -e http://54.40.16.225/engine -R switzerland -t switzerland -r -c ../temp/temp_config
}

paris_upload_switzerland_settings_on_stage() {
  mkdir -p ../temp/temp_config
  \rm -r ../temp/temp_config
  ./tools/upload/configuration-and-dataset.sh -v -l switzerland -p changeit -e http://54.40.18.130/engine -R switzerland -t switzerland -r -c ../temp/temp_config
}

paris_upload_belgium_data_on_test() {
  paris_upload_belgium_data $1 "http://54.40.29.67/engine"
}

paris_upload_belgium_data_on_stage() {
  paris_upload_belgium_data $1 "http://54.40.29.111/engine"
}

paris_upload_switzerland_data_on_test() {
  paris_upload_switzerland_data $1 "http://54.40.16.225/engine"
}

paris_upload_switzerland_data_on_stage() {
  paris_upload_switzerland_data $1 "http://54.40.18.130/engine"
}

paris_upload_austria_settings_on_test() {
  mkdir -p ../temp/temp_config
  \rm -r ../temp/temp_config
  ./tools/upload/configuration-and-dataset.sh -v -l austria -p changeit -e http://54.40.29.180/engine -R austria -t austria -r -c ../temp/temp_config
}

paris_upload_austria_data_on_test() {
  paris_upload_austria_data $1 "http://54.40.29.180/engine"
}

paris_start_postgress_update_server() {
  docker run --rm --name paris_update_server_db -e POSTGRES_DB="update_service" -e POSTGRES_USER="update_service" -e POSTGRES_PASSWORD="password" \
    -p 5433:5432 --volume $HOME/projects/paris/paris_update_server_db:/var/lib/postgresql/data -d postgres:9.6
}

paris_start_postgress_admin_tools() {
  if [[ $1 == '-R' ]]; then
    docker stop paris_admin_tools_db
    \rm -r $HOME/projects/paris/paris_admin_tools_db
  fi
  docker run --rm --name paris_admin_tools_db -e POSTGRES_DB="admin_tools" -e POSTGRES_USER="admin_tools" -e POSTGRES_PASSWORD="password" \
    -p 5434:5432 --volume $HOME/projects/paris/paris_admin_tools_db:/var/lib/postgresql/data -d postgres:9.6
}

paris_start_keycloak() {
  docker run --rm --name keycloak -p 9090:8080 -v $HOME/projects/paris/deployment/files/keycloak/configuration:/opt/jboss/keycloak/standalone/configuration \
    -v $HOME/projects/paris/keycloakData:/data -d jboss/keycloak:2.5.5.Final
}

paris_start_postgress() {
  if [[ $1 == '-R' ]]; then
    docker stop paris_db
    \rm -r ~/projects/paris/paris_db
  fi

  docker run --rm --name paris_db -e POSTGRES_USER="paris" -e POSTGRES_PASSWORD="password" -p 5432:5432 \
    --volume $HOME/projects/paris/paris_db:/var/lib/postgresql/data -d postgres:9.6
}

paris_recreate_postgress_update_server() {
  docker stop paris_update_server
  \rm -r ~/projects/paris/paris_update_server_db
  paris_start_postgress_update_server
}

paris_start_maria() {
  docker run --rm -e MYSQL_DATABASE="paris" -e MYSQL_ROOT_PASSWORD="password" -e MYSQL_USER="paris" -e MYSQL_PASSWORD="password" -p 3306:3306 -d mariadb:10.2
}

paris_recreate_maria() {
  docker_stop_all
  docker run --rm -e MYSQL_DATABASE="paris" -e MYSQL_ROOT_PASSWORD="password" -e MYSQL_USER="paris" -e MYSQL_PASSWORD="password" -p 3306:3306 -d mariadb:10.2
}

paris_decode_aws_message() {
  ~/projects/paris/generate-aws-config/generate-aws-config
  aws sts decode-authorization-message --profile lab --encoded-message $1
}

paris_decode_keycloak_token() {
  node -p "JSON.stringify(JSON.parse(require('atob')('$1'.split('.')[1])), ' ', 2)"
}

paris_upload_4_datasets() {
  wd engine
  paris_upload_local_settings switzerland

  paris_upload_local_data_json ../data/switzerland1 network1_switzerland1

  paris_upload_local_data_json ../data/with_orgs network1_switzerland2

  paris_upload_local_data_json ../data/switzerland3 network2_switzerland3
  paris_upload_local_data_json ../data/switzerland4 network2_switzerland4
}

paris_download_package() {
  \rm -f /Users/tomin/projects/paris/temp/DSC.zip
  curl --user tomin -o /Users/tomin/projects/paris/temp/DSC.zip https://builds.merck.com/job/PROJECTS/job/PARIS2/job/paris2-win-server-build-package-release/lastSuccessfulBuild/artifact/DSC.zip
  # curl --user tomin -o ${TMPDIR}DSC.zip https://builds.merck.com/job/PROJECTS/job/PARIS2/job/paris2-win-server-build-package/lastSuccessfulBuild/artifact/DSC.zip
}

paris_upload_package_to_vitodata_from_temp() {
  filename=${1-DSC_$(date +%Y_%m_%d).zip}
  echo $filename
  duck -v --upload ftps://MSD@ftp.vitodata.ch/KundenDaten/MSD/PARIS2.1/$filename /Users/tomin/projects/paris/temp/DSC.zip
  duck -v -L ftps://MSD@ftp.vitodata.ch/KundenDaten/MSD/PARIS2.1
}

paris_upload_package_to_vitodata_from_jenkins() {
  paris_download_package
  paris_upload_package_to_vitodata_from_temp $1
}