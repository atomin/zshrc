import sys
print(sys.argv)
if len(sys.argv) < 4:
  print("wrong argument 3 arguments are expected - file path, string to replace from and string to replace to")
  exit()

file_path = sys.argv[1]
replace_from = sys.argv[2]
replace_to = sys.argv[3]
print(file_path, replace_from, replace_to)
with open(file_path) as reader:
    file_content = reader.read()
    new_content = file_content.replace(replace_from, replace_to)
    with open(file_path, "w") as writer:
        writer.write(new_content)