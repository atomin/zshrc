#!/bin/bash -v -x

gen-ssh() {
  if [[ ! $1 ]]; then 
    >&2 echo please provide email
  else
    ssh-keygen -t rsa -b 4096 -C $1
  fi
}

who-listens() {
  lsof -ti tcp:$1
}

wcode() {
  tmp=$(pwd)
  wd $1
  res=$(wd $1)
  if [[ ! $res ]]; then
    code .
    cd $tmp
  fi
}

sshconfig() {
  if [[ $1 == '-e' ]]; then
    code ~/.ssh/config
  else
    cat ~/.ssh/config
  fi
}

git-revert-file() {
  firststatus=$(git s | head -n 1)
  file=${firststatus/ M /}
  echo reverting - $file
  git co -- ./$file
}

# duplication from .gitconfig
git-add() {
  prm() {
    if [ -z $1 ]; then
       echo $2
    elif [[ ${1:0:1} == '-' ]]; then
       echo $1
    else
       echo ${GIT_PREFIX:-./}$1
    fi
  }
  git add $(prm $1 .) $(prm $2) $(prm $3) $(prm $4) $(prm $5) $(prm $6)
  git status
}

yarn-reinstall() {
  \rm -rf node_modules
  yarn install
}

watch() {
  fswatch -o $1 | xargs -n1 -I{} sh -c $2
}

npm-versions() {
  yarn info $1 --json | jq '.data.versions'
}


# ---- WR ------
wr-calc() {
  calc=$(node -e "require('/Users/atomin/projects/WR/myhelpers/generateCybersourceSession').getCalculation()")
  echo https://localhost:3443/en/calculation/$calc
  open https://localhost:3443/en/calculation/$calc
}

wr-calc-stage() {
  calc=$(node -e "require('/Users/atomin/projects/WR/myhelpers/generateCybersourceSession').getCalculation()")
  echo https://staging.worldremit.com/en/calculation/$calc
  open https://staging.worldremit.com/en/calculation/$calc
}
